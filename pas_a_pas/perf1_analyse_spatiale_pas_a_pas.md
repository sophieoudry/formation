![SQL_averti](images/image001.png)

# perfectionnement 1: analyses spatiales


**Pré-requis**

- Avoir obligatoirement suivi la formation **SIG 1: Découverte** & **SIG 2: Figures**.
- Il est recommandé d'avoir suivi **Stat1: Statistiques descriptives univariées**
- Utiliser régulièrement et fréquemment QGIS
- Être capable de créer un jeu de données “propre”, tant sur le plan attributaire que géométrique.

**Objectifs principaux** 

- Articulation entre l’enregistrement descriptif et le SIG; concevoir/mettre en oeuvre le système d’enregistrement par rapport à la problématique
- Acquérir les bases de l’analyse spatiale.
- Représenter des données quantitatives et qualitatives.
- Maîtriser les bases du langage SQL.

**Objectifs secondaires**

- Acquérir des automatismes
- - Group Stat
  - Boîte à outils du menu traitement
- Savoir s’orienter dans le langage SQL
- - comprendre une requête
  - chercher de l’aide sur une fonction

**Supports**

Diaporama sur (https://slides.com/archeomatic/perf) ajouter [/live](https://slides.com/archeomatic/perf/live) pour la diffusion synchronisée et en direct lors de la formation. 
> Se déplacer dans le diaporama  avec les touches ↑↓→← du clavier et la touche [Echap.] pour voir l'ensemble.

Ce support de formation mis à jour sur (https://gitlab.inrap.fr/formation/perf) sous licence ![CC](images/icon_CC.png)

**Jeux de données**

- **F103066_Obernai**: csv wkt ; nettoyage, consolidation des données ; cercles proportionnels ; diagrammes en oursin
- **F103455_Esvres** : Correction de géométrie / Script 6 couches / SQL requête / Calcul d’orientation / Ensemble de 
- **F101055_Chilleurs_aux_bois** : lecture
- **F108633_Saran** : sqlite / spatialite
- **F025228_Alizay** : analyse par maille


# Reprise en main de QGIS

Options du logiciel, encodage, paramétrage...

## 0.1. Rappel des fondamentaux des SIG (si nécessaire… )

paramétrage projet:

- paramétrages de QGIS pour les projets à venir
- paramétrage du projet en cours
- - vérifier SCR (2154)
  - chemins d’accès
  - demander le SCR pour les nouvelles couches

projection /reprojection → soit exporter, soit boîte à outils de traitement

## 0.2. Le dossier .qgis3

Le dossier **.qgis3** est créé par QGIS lors de son premier démarrage et comprend un certain nombre de dossiers « annexes » à l’installation, comme le dossier *project_templates* qui stocke les modèles que vous avez créés, le dossier *python* qui stocke les extensions chargées, etc…
Ce dossier peut être également alimenté avec vos propres fichiers ressources (symboles SVG, polices, …). 
Il est conseillé de mettre systématiquement les fichiers ressources (svg, modèles de mises en page, qml..) dans ce dossier.qgis3, afin de faciliter la sauvegarde de tous ces éléments, ainsi que leur utilisation dans des modèles.
Ce dossier est commun à toutes les versions successives de QGIS. Il ne doit donc pas être supprimé.

dans l’arborescence Windows
```
Windows 7 et 10 
C:\Users\compteUtilisateur\AppData\Roaming\QGIS\QGIS3\profiles
```
Par défaut, les extensions, les modèles, les modèles de composeur… sont situés dans ce répertoire. Il peut être copié d’un poste à l’autre pour conserver un certain nombre de réglages associés au logiciel.

Pour y accéder  depuis QGIS: Préférences > Profils utilisateurs > Ouvrir le dossier du profil actif

![profil_utilisateur](images/image002.png)

## 0.3. Projection / Reprojection

**Projection** : mise à plat d’une partie ou de la totalité de la surface courbe de la terre.

**Système de coordonnées de référence (SCR)** : modèle mathématique permettant, grâce aux coordonnées, de faire le lien entre un endroit réel sur terre et sa représentation sur plan. Le choix du SCR est important : il dépend de la taille de la zone de travail, des analyses que l’on veut en tirer… 

Dans le cadre de notre travail quotidien, il est fortement recommandé (obligatoire) de travailler en **RGF93/Lambert 93 (EPSG:2154) → système de projection légal en France.**

### 0.3.1 Projection à la volée

Pour activer/désactiver dans QGIS3 : Projet ⇒ Propriétés ⇒ SCR ⇒ cocher “aucune projection” pour désactiver la reprojection.

![projection_av](images/image003.png)

![attention](images/attention.png) La désactivation de la reprojection à la volée peut poser des problèmes dans le composeur d’impression (problème d’échelle, de graticules…). Il est donc fortement déconseillé de désactiver la reprojection à la volée.

### 0.3.2. Reprojection de couche

Rappel de SIG 1: Découverte:  

Il est possible que le fichier transmis par un collègue ne soit pas projeté dans le même SCR que le projet sur lequel vous travaillez (certains topos utilisent les SCR en conique conforme pour pallier les problématiques de déformation). Il convient dans ce cas d’effectuer une re-projection de couche (autrement dit, effectuer une transformation d’un SCR vers un autre SCR). Pour cela, le logiciel Qgis comprend des outils de re-projection.

> Menu Traitement ⇒ Boîte à outils ⇒ outils généraux pour les vecteurs ⇒ reprojeter une couche. 



# 1. Articulation des données spatiales et descriptives

**objectifs :** rappels des concepts fondamentaux de l’enregistrement stratigraphique communs à tous les systèmes d’enregistrement quels qu’ils soient; restituer les systèmes d’enregistrement de terrain à partir de la lecture des jeux de données; passage en revue circonstancié des différentes solutions existantes pour gérer les données spatiales et les données stratigraphiques.

## 1.1. Conceptualisation enregistrement / Comprendre un jeu de données

### 1.1.1. Rappels historiographiques et conceptuels

- L’enregistrement archéologique est une démarche scientifique dont l’objectif est de conserver la mémoire des vestiges fouillés (mobilier, immobilier, sédimentaires…).

- C'est une simplification du monde réel qui permet une restitution argumentée et interprétée de la stratification anthropique et naturelle fouillée, donc détruite.

- Les principes de l’enregistrement archéologique ont été édictées en Angleterre dans les années 60-70 et sont fondés sur le principe de superposition des strates géologiques mis en place au 17e siècle (et non remis en cause depuis).

- Les principes de l’enregistrement stratigraphique sont diffusés en France dès le début des années 70 et appliqués sur quelques sites emblématiques (Tours, Paris, Lattes...); matrice de Harris développée ensuite .

- La diffusion du « modèle harissien »  est incomplète dès le départ, en France.

### 1.1.2. Plusieurs approches (entrée par l’objet, l’US, la structure)

**Les différentes approches méthodologiques (stratigraphiques et spatiales)** pratiquées par les archéologues en fonction des périodes, des problématiques, des habitudes, ne sont pas contradictoires, mais elles s’insèrent dans un schéma logique de données FAIT ↔ US ↔ Artefact.



De manière résumée et peut-être un peu réductrice, 3 grandes approches cohabitent depuis une bonne trentaine d’années (d’après [Desachy 2008](https://tel.archives-ouvertes.fr/tel-00406241v2)) :

- l’analyse spatiale large : fouille en grand décapage dans la lignée des travaux des protohistoriens
- l’analyse spatiale fine type « ethnographique » (en référence à Leroi-Gourhan)
- un niveau médian : l’analyse par US pour les stratifications d’origine naturelle et/ou anthropique denses

*Avant les SIG*, l’accès à la vision en plan passait uniquement par le plan dessiné/illustré (plan masse, minute) : le plan illustrait le discours et les descriptions. Les spécificités morphologiques et topographiques étaient prises en compte mais pas forcément de manière systématique, dynamique et structurée.

*Avec la généralisation des SIG*, on passe de l’illustration au traitement de données : **la forme que l’on dessine dans l’espace devient elle-même une “donnée” qui n’est plus uniquement définie par sa description mais se définit aussi par des coordonnées et une position relative par rapport aux autres structures**.

D’un point de vue très concret, l’introduction des SIG a permis de prendre en compte et d’exploiter l’espace avant même sa description. Si cette manière de faire convient bien aux pratiques déjà en vigueur dans le cadre d’une “analyse spatiale large” et dans une moindre mesure (?) à l’analyse spatiale fine dite “ethnographique” où l’espace est une entrée privilégiée, elle “convient” moins aux opérations en milieu densément stratifié où la recherche/description/compréhension de la structure précède généralement sa représentation spatiale. C’est là que se pose plus spécifiquement la question de l’articulation entre l’enregistrement stratigraphique descriptif et l’enregistrement spatial (problème des unités qui n’ont pas de représentation spatiale).

Et quand les trois cas de figure se présentent sur un même chantier, il faut "jongler". Charge à l’archéologue de s’adapter et d’adapter son enregistrement spatial et descriptif à la stratification et la densité des structures.

### 1.1.3. Le coeur de tout système d'enregistrement archéologique:  la trilogie *Fait, US, artefact*

**le  coeur de tout système d’enregistrement archéologique** est la trilogie FAIT/US/artefact. 

> Restituer un MCD/MLD sommaire ensuite (1. le coeur de tout système, 2. le MCD simplifié d’une opération, 3. le MPD définissant les tables, les champs - dont clés primaires - et les relations).

Dans tous les systèmes, le coeur est le même : artefact, us, fait. Ensuite, on ajoute les unités techniques d’enregistrement voire des unités de regroupement archéologique. 

**Etape 1 : le coeur de tout système d’enregistrement**

![MCD_simple](images/image004.png)

On peut associer US et Fait, US-artefact mais on ne peut pas avoir un enregistrement avec des Faits, des artefacts, sans US.

> **Notes:**
>
> **MCD** : un **modèle conceptuel de données** a pour but de représenter graphiquement et de la façon la plus simple et la plus directe possible l’ensemble des données qui seront analysées tout en s’affranchissant de toutes les contraintes logicielles ou liées à la base de données sur laquelle va reposer l’application. Dans le MCD, on décrit les relations avec des mots. Formalisme entité-relation.
>
> - permet de modéliser la sémantique des informations d'une façon compréhensible par l'utilisateur de la future bdd
> - utilise le formalisme  *entité-relation*
> - ne permet pas l'implémentation informatique de la bdd dans un SGBD
> **MLD** : un **modèle Logique de données**
> - permet de modéliser la structure selon laquelle les données seront stockées dans le future bdd.
> - est adaptée à une famille de SGBD (ici relationnel).
> - utilise le formalisme Merise.
> - permet d'implémenter la bdd dans un SGBD.
> **MPD** : un **modèle physique de données** est un modèle directement exploitable par la base de données utilisée. C’est le bazar réalisé. Le passage du MCD au MPD peut se matérialiser par la création de tables intermédiaires dans le cas des relations de n à n (cf. ci-dessous).

**Etape 2 : MCD sommaire**

On ajoute les unités techniques; puis ajouter les relations pour établir le MCD

![MCD_sommaire](images/image005.png)

**Etape 2bis: intégrer l'espace**

Ajouter la géométrie des unités qui vont être représentées dans le système d'enregistrement: *points, lignes, polygones*

**Etape 3: MLD/MPD**

Définition des tables, des occurences et des liens de cardinalités.

![MLD_MPD](images/image006.png)

> **Rappel sur les relations** : 
>
> - **relation de 1 à 1** : signifie que pour chaque enregistrement d’une table il ne peut y avoir que 0 ou 1 enregistrement d’une autre table qui lui soit lié (ex : entre la table des faits et une table mobilier…). Dans une base de données classique, ce type de relation peut être évitée en la remplaçant par une la fusion (jointure ?) des deux tables en relation. 
> - **relation de 1 à** **n** (de un à plusieurs) : signifie que pour chaque enregistrement d’une table, il peut y avoir un ou plusieurs enregistrements d’une autre table qui lui soit lié (ex : entre la table des faits et la table des US).
> - **relation de** **n** **à** **n** (de plusieurs à plusieurs) : une relation existe quand un ou plusieurs enregistrements d’une table peuvent avoir une relation avec un ou plusieurs enregistrements d’une autre table (ex : entre la table des faits et la table des photos, où un fait peut être visible sur plusieurs clichés et où l’on peut voir plusieurs faits sur une photo). Dans le cas où l’on souhaite établir une relation de plusieurs à plusieurs entre deux tables, une troisième table est nécessaire pour stocker les combinaisons créées par la relation. Ce type de relation (*n* à *n*) revient à établir deux relations de un à plusieurs (1 à *n*). 

## 1.2. Les outils existants pour exploiter les données spatiales et descriptives

Différentes solutions techniques existent pour interroger conjointement données spatiales et données descriptives. 

On peut simplifier en évoquant 2 cas de figures distincts :
- soit il s’agit d’une combinaison entre QGIS et quelque chose et entre les deux, il y a de la "couture" à faire ;)

- Soit, tout est géré au même endroit.
  Évidemment chaque solution a ses inconvénients et ses avantages.  Du plus abordable (jointures, relations, spatiaLite) a plus complexe (PostGIS, l'exemple de Caviar).

  --------------------------------------

**![important](images/icon_important.png) Pendant la formation nous allons tester différentes solution. Le manipulations ci-dessous seront de 3 sortes:**

* **Démonstration**: seul le formateur fait les manipulations.

* **Exercice dirigé**: le formateur fait la manipulation puis les participants le font sur leur poste dans la foulée.

* **Exercice en autonomie:** Les participants (re)font les manipulations, puis le formateur fait une correction commentée.**

---------------------------------------------------

  

### 1.2.1. Les différents types de stockage des données : *.shp, .csv/wkt*
#### 1.2.1.1. Le format *.shp*
bref rappel de SIG 1: Découverte (4, 5,ou 6 fichiers…), mais on sait faire !
#### 1.2.1.2. Le format *.csv* avec stockage des géométries en *WKT*
> Rappel: Les fichiers CSV sont des formats d’échange commun entre les logiciels gérant les données tabulaires et sont également facilement produits manuellement avec un éditeur de texte ou avec un programme ou des scripts écrits par l’utilisateur.

Le stockage des géométries en WKT (*Well Known Text*) s’opère dans un fichier en texte délimité, dans un champ spécifique où les géométries sont décrites de la manière suivante :  **TYPE DE GEOMÉTRIE (coordX CoordY des noeuds, X Y, X Y,...)** par exemple `POLYGON((1 1,5 1,5 5,1 5,1 1))`


> *Exercice DIrigé*
> **Enregistrer en WKT:**
>
> ⇒ Ouvrir le fichier **Obernai F103066\F103066_vecteur_operation\F103066_poly.shp**
>
> ⇒ Clic droit / Exporter > Sauvegarder les entités sous...
>
> ![save_csv](images/image007.png)
>
> 1. Format “Valeurs séparées par une virgule [CSV]”
>
> 2. Renseigner le nom et le chemin d’accès au fichier **F103066_poly.csv**
>
> 3. Vérifier que le SCR correspond bien au Lambert 93 (EPSG 2154)...
>
> 4. … l’encodage à l’UTF-8
>
> 5. Dans les options de la couche, il faut choisir d’exporter la géométrie comme WKT
>
> 6. On peut ensuite choisir de créer un fichier .csvt (le fichier csvt associé au csv permet de conserver le formatage des champs lors de l’importation du csv créé)[^1]
>
>  7. On peut choisir de ne pas exporter tous les champs
[^1]: Dans QGIS, le module OGR gère la lecture et l’écriture de données essentiellement tabulaire non-spatiale dans des fichiers CSV texte. Lors de la lecture d’un champ nommé “WKT” (qui est supposé contenir une géométrie WKT), les informations stockées sont également traitées comme un champ normal. Le pilote CSV renverra tous les attributs des colonnes avec un type ‘‘chaîne de caractère’’ si aucun fichier d’information sur le type des champs (avec l’extension .csvt) n’est disponible.

Inversement, vous pouvez importer un fichier d’archive en **format WKT** dans un SIG :

> *Exercice dirigé*
> **Importer en WKT:**
>
> ⇒ Aller chercher le fichier **F_103066_poly.csv** via l'icône **![icon_csv](images/icon_csv.png)**
>
> ![import_csv](images/image008.png)

Résultat:
![import_csv2](images/image009.png)

Note: Ici, en ouvrant la table attributaire, vous observez que les champs ont bien leur format d’origine (puisqu’on avait créé un fichier .csvt lors de la création du CSV) : les valeurs d’un champ ‘‘texte’’ sont alignées à gauche ; les valeurs d’un champ ‘‘chiffre’’ sont alignées à droite. 

> Rappel sur le vocabulaire : chiffres entiers (*Integer*), chiffres réels (*Real*), chaînes de caractères (*String*), dates (*Date : YYYY-MM-DD*)

### 1.2.2. Jointures

**RAPPEL du niveau 1**
Qu'est ce qu'une jointure ?
Relier deux tables de données. Relation de 1 à 1 uniquement (à 1 objet dans une table correspond 1 objet dans l’autre table). Nécessite un champ commun (clé primaire).

**Avantages :**
* Très facile à mettre en en oeuvre dès lors que **les tableaux sont bien structurés** (que ces tableaux soient une extraction depuis une base de données ou saisis directement dans un tableur)
* Une modification dans *MS Excel* d'une feuille **.xls** sera répercutée dans QGIS après réouverture du projet.
* Le format  **.odt** (format natif du tableur *LibreOffice Calc*) permet un dynamisme dans les 2 sens (on peut le modifier directement dans QGIS). ![attention](attention.png) le tableau ne doit pas être ouvert en même temps avec *LibreOffice Calc*.

**Inconvénients :**
* Selon le format du fichier il n'y a pas de lien dynamiques donc pas de mise à jour automatique entre les tables de données dans le SIG la plupart du temps.
* Contraint par la relation de 1 à 1

>*Exercice dirigé pour rappel...*
>**Jeux de données**
>Chilleurs-au-Bois
>
>**Pas à Pas**
>
>Étape 1:
>
>* Joindre la couche **F101055_poly** avec le fichier **CADOC_fait_SIG.xls**
>
>* Enregistrer le projet
>
>* Modifier l’enregistrement du Fait 1000 dans *MS Excel*
>
>* Rouvrir le projet et la jointure
>
>* Cela fonctionne dans un seul sens xls → qgis et nécessite de fermer le tableau pour voir les modifications
>
>Étape 2:
>
>* Dans *LibreOffice Calc* Convertir le xls en .ods et fermer *LibreOffice*
>
>* Importer l'ods dans QGIS et  refaire la jointure avec l’ods
>
>* Dans *Calc* modifier la table (changer des valeurs), enregistrer, fermer le tableur.
>
>  ⇒ Dans QGIS, constater que la modification a été prise en compte
>
>* Dans QGIS modifier des valeurs dans la table
>
>  ⇒  Dans *Calc*, constater que la modification a été prise en compte
>
>  Note: Il faut pour valider les modifications faites dans un logiciel fermer/réouvrir le projet (le tableau dans *Calc* / le *projet * dans QGIS).

**Précisions:**

* Une jointure est modifiable depuis la couche mère si les deux couches sont en mode édition.
* Lors du paramétrage de la jointure l'option *mettre la jointure en mémoire virtuelle* : permet de garder la jointure active dès l’ouverture du projet.

### 1.2.3. Les relations dans QGIS

La mise en place de relations 1 à n (1 à plusieurs) entre couches/tables est possible dans QGIS et doivent être paramètrées au niveau du projet.
>Notes: Il est question de relation **attributaire** entre une *Table Mère* et une *Table Fille*. Cette relation grâce à **une clé primaire  ![pk](images/icon_pk.png) dans la *Table Mère*** que l'on peut retrouver en tant que **clé étrangère ![fk](images/icon_fk.png) dans la *Table Fille***. On ne parlera ici plus que de **table** que ce soit pour désigner un tableau ou la table attributaire d'une couche.

**Avantages :** rend la donnée plus accessible d'une table depuis une autre table en relation (Table Mère ↔ Table Fille)

**Inconvénient :** 

* La relation est contenue le projet QGIS uniquement.
* Elle ne permet que de consulter la donnée.

>*Exercice dirigé* 
>**Jeu de données :**
>Saran
>
>**Préalable:**
>Ajouter la couche **F108633_fait** (shapefile) et le tableau **Inv_US.xls**. ![attention](images/attention.png) penser à le passer en UTF-8
>
>**Objectif :** 
>Il s’agit ici de mettre en relation les deux couches, en considérant les caractéristiques suivantes : 
>**Table mère = F108633_fait** → clé primaire ![pk](images/icon_pk.png) **"num_fait"**
>**Table Fille = Inv_US** →  clé primaire ![pk](images/icon_pk.png) **"num_us" **et clé étrangère ![fk](images/icon_fk.png) **"num_fait"**
>⇒ Le champ à mettre en relation est donc **"num_fait" présent dans les 2 tables**.
>
>**Pas à Pas:**
>Menu Projet ⇒ Propriétés du projet
>![proprietes_projet](images/image010.png)
>
>1. Onglet [Relations]
>2. Cliquer sur [![+](images/icon_plus.png)Ajouter une relation]
>![fenêtre ajouter une relation](images/image011.png)
>1. **Nom:** Donner un nom court et explicite à la relation, ici **fait_us**
>2. **Id: ** recopier le même nom
>3. **Couche référencée (parentale):** Choisir la table mère, ici **F108633_fait**
>4. **Champ référencé:** Choisir le champ correspondant à la clé primaire ![pk](images/icon_pk.png) de la table mère
>5. **Couche de référence (Enfant):** Choisir la table fille, ici **F108633_fait**
>6. **Champ de référencement:** Choisir le champ correspondant à la clé étrangère ![fk](images/icon_fk.png) de la table fille qui permet de se "rattacher" à la table mère, ici **"num_fait"**
>7. Enfin laisser la **Force de la relation** sur *Association*

![attention](images/attention.png) Les relations une fois créées ne sont plus éditables: pour les modifier, il faut les supprimer et les refaire.

> **Objectif:** Pour observer la relation dans la consultation (via l'outil information ou directement via la table attributaire) sous la forme de formulaire:
>
> **Pas à Pas:**
>
>  * Menu Projet ⇒ Propriétés du projet
> * Onglet [![icon_form](images/icon_form.png)Formulaire d'attributs]
>  ![formulaire par glisser déposé](images/image012.png)
>  1. Choisir la **Conception par glisser/déposer**[^2]
>
>  2. Ne garder que les champs "num_fait", "typoly" et "ident" ainsi que la relation "fait_us"
>
>  3. Pour ajouter les champs de la couche US : cliquer sur ![+](images/icon_plus.png)...
>
>  4. ... et renseigner dans la fenêtre qui s'est ouverte: créer une catégorie = **US** en tant que **onglet**
>
>     puis glisser la relation (fait→us) dans le groupe créé.
>
>     ![formulaire_par_glisser_déposer_suite](images/image013.png)
>     Valider avec [OK]


[^2]: Il existe 3 modes de création de formulaire :  `Génération automatique`: garde la structure basique "une ligne - un champ" pour le formulaire mais permet de personnaliser chaque *widget* correspondant (per ex. faire une boite à cocher pour les champ en *1/0*, ouvrir un calendrier pour les champs *date*, etc) `Conception par glisser/déposer`: outre la personnalisation des *widgets*, la structure du formulaire peut-être complexifiée avec des regroupements de ceux-ci en groupes ou en onglets par ex.
`A partir du fichier .ui fourni`: Permet l'utilisation d'un fichier .ui créé avec le logiciel *Qt designer* et qui permet par conséquent une personnalisation plus avancée *mais avec des limitations qui peuvent être rédibitoires comme l'impossibilité de passer simplement d'un enregistrement à l'autre[ndlr]

⇒ Dès lors , si on identifie un fait on observe les US en relation avec le fait.

> **Objectif:** Accéder au formulaire nouvellement créé
>
> **Pas à Pas:**
> ![formulaire](images/image014.png)
>
> * Ouvrir la table attributaire de la couche **F108633_fait**.
> * Passer en mode  *Formulaire* grâce au bouton dédié ![icon_form](images/icon_form.png)en bas à droite de la fenêtre.
> * Dans le menu déroulant Expression  ⇒ Prévisualisation de colonne ⇒ "numfait"

On peut déjà utiliser cette petite mise en page de formulaire pour exploiter la relation et faire une recherche. En revanche on ne peut pas faire de requête exploitant cette relation. On verra plus tard [les requêtes SQL exploitant des relations](#2-1-2-jointure-de-1-à-n).

Pour la consultation, il est possible de:

* passer par la **barre d'outils des attributs** ⇒ Sélectionner des Entités par Valeur

  ![selection_valeur](images/image015.png)

* depuis le formulaire choisir l'icône Filtre

  ![formulaire_icone_filtre](images/image016.png)

### 1.2.4. Spatialite

L'utilisation d'une **base de données (BDD) relationnelle intégrée et interfacée dans QGIS** est possible ! Elle utilise le moteur de base de donnée relationnel **SQLite**.

![important](images/icon_important.png) Cette solution fait l'objet d'une formation dédiée: [**SIG perfectionnement 2: Base de Données relationnelle attributaire et spatiale** ](README.md) nous ne faisons donc ici qu'une démonstration du fonctionnement d’une BDD relationnelle SQLite/spatialite dans QGIS.

**Principe :** 

**SQLite** en quelques points:
* C'est un moteur de BDD relationnelle OpenSource, accessible par le **langage SQL**. 
* **Une base SQLite = 1 fichier** autonome, très léger et indépendant du système d'exploitation (contrairement à *MS Access* ou *FileMakerPro*) . 
* la BDD contient les tables, les relations, les requêtes SQL,des index,des  fonctions…

**Spatialite** est une extension de SQLite qui:
* Permet de gérer les géométries spatiales, toujours dans un seul fichier**.sqlite**

* L’extension Spatialite de SQLite est intégrée par défaut dans QGIS.

  

**Avantages :**

- Gestion et exploitation des données dans un seul et même outil

- Interopérabilité avec le reste du monde

- Facile à utiliser, *permet de s'initier réellement au langage SQL [ndlr]*

- Mono-utilisateur, il n'y a pas d'administration de BDD à faire

- Les styles peuvent être enregistrés dans la base 

  >  Note: il faut passer par le gestionnaire de connexion (Data source manager) ou par le **DB Manager** pour qu’ils soient appliqués. PAS CLAIR ??
  
  ![gestionnaire_de_BDD](images/image017.png)



**Inconvénients:**

- Ne gère pas les rasters

- Mono-utilisateur

**Démonstration:**

>**Jeu de données :** 
>
>Fouille de Saran, “La Motte Pétrée”, RO : Laurent Fournier.
>
>1 projet QGIS: **F108633_Saran_Motte_Petree.qgs** et  un fichier:**F108633_Saran_Motte_Petree.sqlite**
>
>On commence donc par exploiter un fichier/base de données préexistant.
>Ouvrir le projet QGIS **F108633_Saran_Motte_Petree.qgs**



Tout est déjà fait ! (et tout tient dans 2 fichiers):

* Le fichier QGIS gère uniquement l’affichage de la relation inter-table.

* Le reste est défini et administré par la base de données SQLite.

* Cela se gère comme on a l’habitude : un projet avec des couches.




### 1.2.5. PostGis (CAVIAR)

[**PostGis**](http://www.postgis.fr/) est une extension du Système de Gestion de Base de Données Relationnelle (SGBDR) [**PostgreSQL**](https://www.postgresql.fr/). C'est la cartouche spatiale de Postgresql à l'instar de Spatialite pour SQLite.

Le **CA**talogue de **Vi**sualisation de l'information **AR**chéologique (**CAVIAR**) est hébergé sur un serveur PostgreSQL/PostGis.

**Avantages :**
- C'est un système très puissant conçu pour la gestion de volumes de données importants (d'ou l':elephant:)
- On peut tout faire avec l'association Postgre/PostGis: manipuler des données descriptives et attributaires, "mélanger" les géométries, faire des vues...
- C'est une solution serveur qui permet l’édition et la consultation simultanée par plusieurs utilisateurs et depuis différentes interfaces (clients).

**Inconvénients :**
- Difficile d’utilisation
- Nécessité d'être administrée depuis une interface spécifique

**Jeux de données** :
Caviar

> *Exercice  dirigé (consultation)*
> **Pas à Pas**: 
> - Ouvrir QGIS si ce n’est déjà fait (mais si c’est pas fait, c’est inquiétant...)
> 1. Créer une nouvelle connexion à une base de données PostGis en cliquant sur l'icône dédié ![icon_pg](images/icon_pg.png)qui ouvre le *Gestionnaire de sources de données* (= *Data Source Manager*)
>
> 2. Cliquer sur [Nouveau] afin de paramétrer une nouvelle connexion
>
> 3. ![connexion_pg](images/image028.png)
>
>   ![param_connexion_pg](images/image029.png)
>
> - Renseigner les les paramètres suivants :
>   * Nom : **CAVIAR**
>   * Hôte : **10.210.1.32**
>   * Port : **5432**
>   * Base de données : **activite**
>   * nom d’utilisateur : **agent**
>   * mot de passe : **agent**
>   
> - De retour dans le *gestionnaire de source de données* :
>   - Cliquer sur [connecter]
>   - dérouler la base **activite** (clic sur ►)
>   - Ajouter les couches désirées  en double-cliquant dessus ou  1 clic + [Ajouter]
>   
>
> ![attention](images/attention.png) Quelques subtilités:>
> * Les couches sont disponibles pour "toute la France" ou par DIR.
> * Si la couche est précédée d'un icône ![attention](images/attention_color.png): dans la rubrique **id de l'entité**, il faut choisir l'identifiant unique de la table **gid** .
> * PostGis pouvant stocker des couches avec des géométries différentes il est possible que l'on vous demande de choisir quelle géométrie affiché (pour les couches **uniteobservation** par exemple).


### 1.2.6. Requête et SQL : Le SQL c’est quoi et ca sert à quoi ?

Tout ce qui concerne le SQL dans ce déroulé est compilé dans le document [memoSQL](/memoSQL/memoSQL.md) 

#### 1.2.6.1. Do You Speak SQL ?
[source](http://www.commentcamarche.net/contents/1062-le-langage-sql)

Le **SQL** (Structured Query Language, traduisez Langage de requêtes structuré) est un **langage de définition de données, un langage de manipulation de données et un langage de contrôle de données** pour les[ bases de données relationnelles](http://www.commentcamarche.net/contents/105-les-modeles-de-sgbd). 

- Le SQL est un langage de **définition de données**, c'est-à-dire qu'il **permet de créer des tables dans une base de données relationnelle, ainsi qu'en modifier ou en supprimer**.
- Le SQL est un **langage de manipulation de données**, cela signifie qu'il **permet de sélectionner, insérer, modifier ou supprimer des données** dans une table d'une base de données relationnelle. 

Il est possible avec SQL de définir des permissions au niveau des utilisateurs d'une base de données.

![important](images/icon_important.png) En fait, le langage SQL peut rendre tous les services que l’on peut demander à une BDD relationnelle: Créer la structure de la BDD (créer des tables, créer les champs, remplir les données mais aussi créer les liens entre les tables et **SURTOUT interroger la BDD (C’est d’ailleurs essentiellement pour cela que nous allons l’utiliser)**. C’est ce que vous savez déjà tous faire (plus ou moins) sans forcément vous en rendre compte.

Avant tout le SQL est donc un langage de requête structuré il respecte donc une **syntaxe générale** de type (pour sa partie liée à l’interrogation et à la manipulation de données existantes) :

**SELECT** **liste des champs** **FROM** **liste des tables** **WHERE** **conditions**

- La partie **SELECT** indique les champs qui doivent apparaître dans la réponse. 

> Note: ces champs sont soit des champs existants dans les tables déclarées derrière la clause FROM soit le résultat d’un calcul. Il faudra dans ce cas leur donner un nom.

- La partie **FROM** décrit les tables qui sont utilisées dans la requête.

- La partie **WHERE** exprime les conditions, elle est optionnelle.

![important](images/icon_important.png)Quand vous faites une sélection dans QGIS la fenêtre expression correspond aux conditions c’est à dire ce qui suit la clause **WHERE**. Il faut imaginer que le logiciel fait précéder votre expression de la phrase :

**SELECT** ***** **FROM** **couche en cours** **WHERE**

> Où **\*** signifie **tous les champs**



**Les clauses SQL :** (clause = Partie d'un ordre SQL précisant un fonctionnement particulier)

| **SELECT**    | Précise les colonnes qui vont apparaître dans la réponse     |
| ------------- | ------------------------------------------------------------ |
| **FROM**      | Précise la (ou les) table intervenant dans l’interrogation   |
| **WHERE**     | Précise les conditions à appliquer sur les enregistrementsOn peut utiliser des comparateurs : =, >, <, >=, <=, <>des opérateurs logiques : AND, OR, NOTles prédicats, IN, LIKE, NULL, ALL, SOME, ANY, EXISTS... |
| **GROUP BY**  | Précise la (ou les) colonne de regroupement                  |
| **HAVING**    | Précise la (ou les) condition associée à un regroupement     |
| **ORDER BY**  | Précise l’ordre dans lequel vont apparaître les lignes de la réponse :ASC : en ordre croissantDESC : en ordre décroissant |
| **LIMIT (n)** | Permet de limiter le calcul au n premiers enregistrements    |
| **DISTINCT**  | Permet d’éviter les redondances dans les résultats (il s’agit d’une option à la commande SELECT). |
| **COUNT()**   | Permet de compter le nombre d’enregistrements dans une table. |



**Les opérateurs de comparaisons**

La clause **WHERE** est définie par une condition qui s'exprime à l'aide d'opérateurs de

comparaison et d'opérateurs logiques.

| A = B                                                        | A égal B                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **A <> B**                                                   | A différent de B                                             |
| **A < B**                                                    | A plus petit que (inférieur à) B                             |
| **A > B**                                                    | A plus grand que (inférieur à) B                             |
| **A <= B**                                                   | A inférieur ou égal à B                                      |
| **A >= B**                                                   | A supérieur ou égal à B                                      |
| **A BETWEEN B AND C**(ne fonctionne pas avec les Virtual Layers) | A est compris entre B et C                                   |
| **A IN (B1, B2,...)**                                        | A appartient à liste de valeur (B1,B2,..)                    |
| **A LIKE 'chaîne de caractères'**                            | permet d’insérer des caractères jokers:% désignant 0 à plusieurs caractères quelconques_ désignant un seul caractère |



**Les opérateurs logiques**

**OR :** pour séparer deux conditions dont au moins une doit être vérifiée.

**AND :** pour séparer deux conditions qui doivent être vérifiées simultanément.

**NOT :** permet d'inverser une condition.

**AND** est prioritaire sur **OR** (en cas de doute, ne pas hésiter à mettre des parenthèses pour prioriser les sous-requêtes.



L'ordre de priorité des opérateurs est indiqué dans le tableau suivant. Un opérateur de priorité élevée est évalué avant un opérateur de priorité basse.

| Niveau           | Opérateur                                                    |
| ---------------- | ------------------------------------------------------------ |
| 1 (niveau élevé) | ~ (NOT au niveau du bit)                                     |
| 2                | * (Multiplication), / (Division), % (Modulo)                 |
| 3                | + (Positif), - (Négatif), + (Addition), + (Concaténation), - (Soustraction), & (AND au niveau du bit), ^ (OR exclusif au niveau du bit), \| (OR au niveau du bit) |
| 4                | =, >, <, >=, <=, <>, !=, !>, !< (opérateurs de comparaison)  |
| 5                | NOT                                                          |
| 6                | AND                                                          |
| 7                | ALL, ANY, BETWEEN, IN, LIKE, OR, SOME                        |
| 8 (niveau bas)   | = (Affectation)                                              |



Lorsque deux opérateurs dans une expression ont le même niveau de priorité, ils sont évalués de gauche à droite en fonction de leur position dans l'expression. 

> *Exercice dirigé .. Rappel*
> **Jeu de données:**
> Saran_BDD.sqlite
>
> **Objectif:** Depuis le *Gestionnaire de BDD*, faire une requête SQL simple.
> Pour savoir comment accéder à la fenêre SQL voir dans le [memoSQL](/memoSQL/memoSQL.md##le-gestionnaire-bd)
> 
> Sélectionner les trous de poteau et les fosses fouillés.

#### 1.2.6.2. Le gestionnaire BD

Menu [Base de données] ⇒ Gestionnaire de BD
![Menu_DBmanager](images/image025.png)

![DB Manager](images/image030.png)
1. Une fois dans le *Gestionnaire de BDD* pour écrire une requête SQL cliquer sur l'icône requête SQL ![icon_requete_SQL](images/icon_reqSQL.jpg)
2. Dans la fenêtre qui s'ouvre tapez la requête SQL:
```sqlite
SELECT * FROM tabFait WHERE ident IN ('Trou de poteau', 'Fosse') AND statut LIKE 'fouillé'
```
puis cliquer sur le bouton ![Exécuter F5](images/bouton_executer.jpg)

![DB Manager](images/image031.png)
Il est possible de stocker et d’exploiter cette requête de différentes manières

1. **Enregistrer la requête** (donner un nom court et expressif):
 ⇒ valable pour le projet en cours.

  * Taper le Nom de votre requête

  * Cliquer sur le Bouton [Enregstrer]

  * La requête est maintena accessible dans le menu déroulant *Requête enregistrée*

2. **Charger en tant que nouvelle couche** : 
 ⇒ valable pour le projet en cours.

  * Choisir du champ contenant la clé primaire (valeur unique)

  * Choisir le  champ contenant les géométries (générale ment "geom"

  * Donner un nom à la nouvelle couche 

  * [Charger]

    ⇒ valable pour le projet en cours

3. **Créer une vue** : 
⇒ enregistrée “en dur” dans la base de données, indépendant du projet en cours

  * Cliquer sur [Créer une vue]

  * Lui donner un nom (explicite et court et qui commence par vue_ par ex.)

  ⇒ La vue résultant de la requête est enregistrée dans la base de données ⇒ elle apparaît dans la liste des éléments de la base de données à gauche
  
>  Si la vue ne s'affiche ne s'affiche pas rafraîchir la vue ![F5](images/icon_F5.png)

![DB Manager](images/image032.png)

![important](images/icon_important.png) Une **vue** dans une base de données est une synthèse d'une requête d'interrogation de la base:

* On peut la voir comme une table virtuelle, définie par une requête. 
* Les enregistrements ne sont pas modifiables directement, il faut modifier la table de départ.
* Avantage : système dynamique, toute modification de la table de départ (ajout, suppression ou modification d’un enregistrement) sera répercutée dans la vue.

#### 1.2.6.3. Manipulation, interrogation de données

![important](images/icon_important.png)Avant de se lancer dans une requête SQL la première chose à faire est de **s’approprier la table attributaire** afin de repérer où se trouvent les informations que l’on recherche. Cette étape indispensable peut se faire de plusieurs façons mais elle intègre toujours un passage en revue de la table.

> Note: Pour les exercices propre à cette formation vous trouverez en fin de document un récapitulatif des tables. **FAIRE UN LIEN**


> *Exercice dirigé*
> **Jeu de données**
> **Présentation de la Fouille d'Esvres (37) **: le contexte [Fiche sur Inrap.fr](http://www.inrap.fr/nouvelles-decouvertes-sur-la-necropole-gauloise-et-gallo-romaine-d-esvres-sur-5308) et le jeu de données.
>
> Fouille d'une nécropole GR. Enregistrement à l'échelle de la sépulture.
> Unité d'enregistrement spatial = le point 
> Unité d'enregistrement descriptif = UE.Ordre qui permet d'enregistrer mobilier, éléments architecturaux et anthropologiques.
> F103455_emprise.shp
> Esvres_poly.shp (essentiellement des fosses de sépultures)
> F103455_axe.shp (Axes pour profil longitudinaux et transversaux)
> F103455_point.shp (contient les points: unité d'enregistrement spatial)
>
> **Pas à Pas**
>
> * Ouvrir la table attributaire de **F103455_point**
>
> * Passer en revue les champs et tenter de qualifier le type de variables (clef primaire ![pk](images/icon_pk.png), éventuelles clefs étrangère![fk](images/icon_fk.png), variables qualitatives -texte-, variable quantitative…),
>
>   ![ex_table_point](images/image033.png)

On observe que la table a été structurée pour décrire les objets de la tombe selon les champs **"Type"**, **Matiere** et  **Type_obj**. Il serait intéressant de lister toutes les occurrences (lister les valeurs uniques) pour chaque champs ou mieux les différentes occurrences d’association entre ces 3 champs :

>**Objectifs: **Lister les valeurs uniques d'un champs.
>
>Menu [Traitements] ⇒ Boîte à outils de traitements ⇒ Analyse vectorielle ⇒ **Liste les valeurs uniques**
>
>![ex_table_point](images/image034.png)
>
>1. Couche à interroger
>2. & 3. Choix des champs dont on veut interroger les valeurs
>
>**Résultat :** 18 combinaisons différentes
>
>![resultat_valeurs_uniques](images/image035.png)

> *Exercice dirigé...Rappel*
> **Objectif:** Identifier/Localiser les points correspondants a des **clous en cuivre** .
>
> 1. D’abord, Il faut avoir sélectionné la bonne couche dans le panneau couche !
> 2. **Pour sélectionner** (résultat sélectionné, peut être copié…, apparaît en jaune sur le canevas de la carte et en bleu dans la table) : ![SQL_icon_selection_expression](images/icon_icon_select_epsilon.png) Sélectionner les entités en utilisant une expression
> 3. Pour filtrer (n’affiche que les entités résultant de la requête sur le canevas et la table) ⁠⁠⁠:
> [Ctrl]+[F]
>
> ![ex_table_point](images/image036.png)
>
> 4. Dans les deux cas, taper la requête :
> ```sqlite
> "Type" LIKE 'Clou' AND "Matiere" LIKE 'Cuivre'
> ```
>![ex_table_point](images/image037.png)
> Note:  Si le panneau valeur (à droite) n’est pas affiché, mettre le curseur à droite du panneau fonction et glisser déposer vers la gauche.



![important](images/icon_important.png) **Rappels:**
- `"Champ”` **opérateur** `'valeur'`  `AND`/`OR` `"Champ”` **opérateur** `'valeur'`  

- Les **champs** sont à mettre entre guillemets (touche[3]) et les **valeurs textuelles** entre apostrophes (touche[4]).

- Ne pas confondre **FILTRE** et **SELECTION**

  

Il est conseillé d’utiliser un logiciel de type [notepad++](https://notepad-plus-plus.org/fr/) pour enregistrer les requête SQL que l’on souhaite garder en mémoire. Menu Langage > SQL.



> *Exercice en autonomie*
>
> **Objectif: ** Identifier les points correspondants à du mobilier en verre
>
> ```sqlite
> Type" LIKE 'Mobilier' AND "Matiere" LIKE 'Verre'
> ```

![important](images/icon_important.png)**Rappel** `ILIKE` (*to move it move it*:musical_note:)

* Correspond à `LIKE` mais ne tient pas compte de la casse 
* ![attention](images/attention.png)n'est pas supporté par le couches virtuelles (*virtual layer*)

### 1.2.7. Virtual Layer

Il est possible dans QGIS d’utiliser le SQL pour créer **des couches virtuelles** (*virtual layer*).

source : [documentation QGIS](http://docs.qgis.org/2.14/fr/docs/user_manual/working_with_vector/virtual_layers.html

Une couche virtuelle:

*  C’est une couche vecteur résultant d’une requête SQL sur une ou plusieurs couches vectorielles
* Elles correspondent à des vues sur d’autres couches et ne stockent donc pas de données
* Elles sont dynamiques: si les couches d’origine sont modifiées ce sera pris en compte par la couche virtuelle résultante.

Les couches virtuelles permettent donc de faire des requêtes sur **n’importe quelle couche ou table présente dans le projet QGIS** pour créer une nouvelle couche. En outre, ces couches sont dynamiques dans le sens ou si les données des couches d’origine sont modifiées ce sera pris en compte par la couche virtuelle résultante (= des vues).

> *Exercice dirigé*
> > **Objectif:** Créer une couche virtuelle de points contenant tous les points correspondant à du **mobilier en verre** 
>
> ![Virtual_layer](images/image038.png)
> 1. **Donner un nom** à la couche que vous allez créer : **points_verre**
> 2. (facultatif) **Importez** les couches que vous allez requêter : F103455_point. Le fait d’intégrer les couches permet de ne pas avoir à les charger dans le projet pour les requêter.
> 3. Tapez une requête SQL 
> ```sqlite
> SELECT N_PT, Type, Matiere, geometry FROM F103455_point WHERE Type LIKE 'Mobilier' AND Matiere LIKE 'Verre'
> ```



**Explication de ~~texte~~ code:**

* **`SELECT`** suivi des champs que l’on veut voir apparaître dans la table attributaire résultante, ici **"N_PT"**,**"Type"**,**"Matiere"** et ne pas oublier **"geometry"**
  ⇒ Comme le résultat est une couche de points il faut ajouter le champ **"geometry"** (il existe dans toutes les couches vectorielles mais n'apparaît pas dans la table attributaire)

> Dans QGIS le champ contenant la géométrie s'appelle **"geometry"** en revanche dans les couches Spatialite ce champ s'appelle **"geom"**

- **`FROM`** suivi du nom de la couche sur laquelle la requête va porter : **F103455_point**

- **`WHERE`** suivi des conditions de la requête comme vous l'écririez dans le panneau expression de l’outil *selection by expression* : `"Type" LIKE 'Mobilier' AND "Matiere" LIKE 'Verre'`

![Attention](images/attention.png)  Quand on commence à taper les premières lettres d’un champ, le logiciel nous propose des noms de champs. On peut choisir le champ désiré et valider avec [Entrée], le nom du champ s’affiche alors en noir et sans guillemets, cela fonctionne généralement MAIS il est conseillé de mettre les noms de champs entre guillemets.

![Attention](images/attention.png) Il faut faire attention aux noms des champs qui ne doivent pas correspondre à des fonctions SQL, éviter “date”, “type” (entre autres)

![Attention](images/attention.png) On pourrait utiliser **\* **pour faire apparaître tous les champs mais il est recommandé de les lister.

![Attention](images/attention.png) Dans le langage SQL on peut “baliser” des commentaires avec les caractères `/*` (début) et `*/` (fin). Ceci peut être très utile pour expliciter les requêtes complexes.

>4. Laisser la case **Autodétecter** cochée ou éventuellement cocher le dernier bouton puis remplir: colonne géométrique = geometry, Type = Point et EPSG = 2154.
>5. Cliquer sur le bouton [**Tester**] pour tester la syntaxe de votre requête SQL

![Attention](images/attention.png) Seule la syntaxe est vérifiée il faudra vérifier le résultat !
>6. Cliquer sur [OK]
>7. **Vérifier le résultat:** 

⇒ la couche doit apparaître dans le panneau couche avec le bon nom (point_verre) et la géométrie associée (un point).
⇒ vérifier la table attributaire: les noms de champs et la présence d’enregistrements.

>8. Si ce n’est pas le cas, on peut revenir sur la requête d’une couche virtuelle par un clic droit ⇒ **Éditer les paramètres de la couche virtuelle** 

⇒ Décocher l’affichage de la couche **F103455_point**: On peut repérer graphiquement les sépultures (**couche F103455_poly**) qui contiennent des objets en verre. 

> Éditer une couche virtuelle contenant les points, leur numéros, les numéros de fait, et le type d’objet des entités en métal qui ne sont pas des clous :

```sqlite
SELECT n_pt, fait, type_obj, geometry FROM F103455_point WHERE Matiere LIKE 'Métal' AND Type <> 'Clou'
```

> Éditer une couche virtuelle contenant la liste des éléments de rivets dont l’altitude est supérieure ou égale à 84,20 m NGF et indiquant leur numéro de point, le fait auquel ils appartiennent, l’identifiant de l’objet auquel ils correspondent et leur altitude

```sqlite
SELECT n_pt, fait, ue, z FROM F103455_point WHERE Type_obj LIKE 'rivet' AND Z >= 84.20
```
⇒ observer, décrire (changement d'icône…)

 ![virtual_layer_icone](images/icon_virtual.png)


## 1.3. Préparation des données

La préparation, la vérification et le nettoyage des données est une étape indispensable avant tout traitement complexe. La plupart des outils nécessaire à cette étape peuvent être trouvé via la **boîte à outils de traitement**.

La boîte à outils propose des outils qui vont servir aussi bien à la préparation/vérification/nettoyage des jeux de données qu’à l’analyse des données.

Quelques outils à connaître:

- analyse vectorielle : liste des valeurs uniques, compter points dans polygones…
- base de données : exporter dans PostgreSQL/SpatiaLite
- création de vecteurs
- géométrie vectorielle
- outils généraux pour les vecteurs

### 1.3.1. Scripts 6 couches

Modèles de structuration de shapefile afin de les structurer en s’inspirant du modèle CAVIAR. Rajoute les champs nécessaires avec les bons noms et types.

> **Objectif:** Installer les 6 fichiers/modèles
>
> 2 manières:
>  ![Attention](images/attention.png) Note : ne pas copier **struct_axe**, il sera fait ensemble
>
> 1. Boîte à outils de traitements ⇒ ajouter un modèle à la boîte à outils ⇒ Choisir les fichiers de modèle un par un pour les installer.
>
>   >  Note: Les  modèles seront copiés automatiquement dans le dossier `C:\Users\[nom_utilisateur]\AppData\Roaming\QGIS\QGIS3\profiles\default\processing\models`
>
> 2. En copiant, à la main, les fichiers de modèle dans le dossier `C:\Users\[nom_utilisateur]\AppData\Roaming\QGIS\QGIS3\profiles\default\processing\models`
>   ⇒ dans ce cas, il faut relancer QGIS pour que l’installation soit effective
>
>   ![ajouter_modele](images/image039.png)



**Explications sur le fonctionnement du modèle :**

Le **modeleur** est un outil qui permet de préparer et paramétrer une chaîne de traitement.

Pour observer un exemple en détail:

* Clic-droit sur le modèle **struct_poly**  *Éditer modèle*

* Dans la fenêtre du Modeleur:
	* A gauche, les options
	* A droite, les traitements du modèle
	* Dans l’onglet [Entrée], les éléments à paramétrer en entrée lorsque l'outil sera lancé.
	* Dans l’onglet [Algorithme], on trouve tous les outils disponibles au travers de la boîte à outils de traitements pour les utiliser dans le modèle.

En cliquant sur le crayon ![crayon](images/icon_crayon.png) à côté de chaque traitement on accède à ses paramètres :

- poly_entree : création d’un couche à partir d’un couche en entrée
- Add numpoly : ajout du champs numpoly de type Entier.
- etc. pour les autres champs
- poly_sortie : nom de la couche temporaire qui sera ajouté dans la liste des couches du projet.

> *Exercice dirigé*
> **Objectif:** Création du modèle de structuration des axes : **struct_axe.model**
> 1. Boîte à outils > créer un nouveau modèle
>    ![modele_new](images/image040.png)
> 2. Renseigner le nom (**struct_axe**) et le groupe (**shp_operation**) auquel appartiendra le modèle.  ![modele_new](images/image041.png)
> 3. Onglet Entrées ⇒ Couche vecteur ⇒ définir les paramètres:
> * Nom du paramètre : **couche_axe** (définit le nom de la couche appelée)
> * Type de géométrie : **ligne** (impose le type de géométrie, ici des lignes)
>    ![modele_new](images/image042.png)
> 4. Onglet Algorithmes ⇒ Table vecteur ⇒ Ajouter un champ à la table des attributs
> * Nom du champ : **numaxe**
> * Type de champ : **chaîne de caractère**
> * Longueur du champ : **10**
>    ![modele_new](images/image043.png)
> 5. Onglet Algorithmes > Table vecteur > Ajouter un champ à la table des attributs
> * Couche source : 'Ajouté' from algorithm 'Ajouter un champ à la table des attributs'
> * Nom du champ : typaxe
> * Type de champ : chaîne de caractère
> * Longueur du champ : 10
> * Ajouté : numSGA_axe
>    ![modele_new](images/image044.png)
> 6. Résultats : pensez à enregistrer le modèle.
> ⇒ Par défaut, il sera enregistré dans QGIS3\*nom_profil*\default\processing\models
>    ![modele_new](images/image045.png)

Vous avez fait votre 1er modèle de traitement ! reste plus qu'à le tester...

>*Exercice dirigé*
>**Objectif:** Tester votre modèle sur une couche  shape
>
>**Jeu de données:**  shapefile des faits (polygones) du site Esvres (Esvres_emprise)
>
>Ouvrir la table attributaire de **Esvres_poly**
>
>**Remarque :** en théorie, on reçoit/récupère un ou plusieurs shapes fournis par le topo. C’est la donnée brute. Cette donnée ne doit pas être modifiée et doit être conservée en l’état. On ne modifie donc pas la structure des shapes bruts et on en crée de nouveaux qui serviront au traitement de la données.
>
>
>
>1. Boîte à outils ⇒ Modèles ⇒ shp_operation ⇒ poly
>	* *poly_entree* : **Esvres_poly**
>	* laisser le reste en l’état
>	* **[Exécuter]**
>	![modele_new](images/image045.png))
>2. **Un nouveau shape (temporaire)** apparaît dans la liste des couches.
>	Ouvrir la table attributaire pour observer les champs et constater la création des nouveaux champs.
>	Dès lors, certains champs peuvent **être mis à jour**. Par exemple, numpoly.
>	Mise à jour soit par la **calculatrice de champ**, soit par la **barre de formule** une fois passé en Mode Édition.
>3. Ne pas oublier d’enregistrer en dur le nouveau shape mis en forme : **F103455_poly**

> :skull: Vous en voulez encore ? Il existe un tutoriel sur le model builder sur [sigterritoires](https://www.sigterritoires.fr/index.php/tutoriel-modeleur-graphique-de-qgis-2-8/)



:skull: **FIN DE LA PREMIÈRE JOURNÉE** :skull:



### 1.3.2. Transformation / consolidation

**Exercice** : Homogénéisation des valeurs au moyen de la calculatrice de champ

**Objectif** : Uniformiser les données attributaires

**Jeu de données** : Obernai_F103066

> Ouvrir la table attributaire de **F103066_poly**, et observer.

Dans le champ "interpret", il y a des valeurs en “US - …” et en “NF - …” (pour “ non fouillé ”) : il faut homogénéiser les valeurs afin d’avoir des cartographies “automatiques” et des statistiques / requêtes justes.

> Il y a ici des informations de nature différente : on va vouloir mettre l’information “US” dans le champ “typoly” ; et l’information “NF” dans un nouveau champ “EtatFouile”. Puis, homogénéiser les valeurs des types d’entités (par exemple, mettre “fosse” pour “US - Fosse”, “NF - Fosse” et “fosse” ; etc.).

>*Exercice en autonomie*
>**Objectif**  Chercher les valeurs uniques du champ **"interpret"**
>
>Boîte à outils de Traitements ⇒ Analyse vectorielle ⇒ Liste des valeurs uniques
>Note: Déjà fait sur les données de Esvres.

Sélectionner les valeurs en “US - …” (à la main ou via ![selection_expression](images/icon_select_epsilon.png)"interpret" LIKE '%US%') dans le champ “interpret”, et saisissez la valeur ‘US’ dans “typoly” au moyen de la calculatrice de champ.

> Sélectionner les valeurs en “NF - …” ( à la main ou via ![img](https://lh6.googleusercontent.com/H81PRmjnVzQDly29Y8qaPLm4RU0OrBZ-_bPQP3j7YHpGBeaTpXfptXa164MzJeTL9OcIlDTW1GrOF-VgKO_up4vixmC9vH0tApxNCSWQ1oIIVlkPTmluGQKGbrABKntw5FKs9Dnx):  "nterpret" LIKE '%NF%' `) dans le champ "interpret", et créer un nouveau champ "EtatFouile", en y appliquant, pour les valeurs sélectionnées préalablement, la valeur 0 (non fouillé) :

Inverser la sélection![icon_inverser](images/icon_inverser.png), et appliquer la valeur 1 (fouillé) pour les autres valeurs

> Autre méthode, directement dans la calculatrice de champ, sans sélection préalable : créer un champ EtatFouille et y appliquer les valeurs correspondant à la requête suivante
```sqlite
`IF("interpret" LIKE 'NF%', 0, 1)`
```
> ![exo](images/icon_exo.png) *Exercice en autonomie* 
>
> **Objectif:** Mettre à jour le champs **"typoly"**, **"US"** et **"Fait"**
```sqlite
 IF("interpret" LIKE 'US%', 'US', 'fait')
```

>![exo](images/icon_exo.png) *Exercice en autonomie* 
>
>**Objectif:** Trouver une solution pour mettre à jour le champ **"interpret"**

```sqlite
CASE   WHEN "interpret" LIKE 'US -%' THEN replace("interpret", 'US - ','')   WHEN "interpret" LIKE 'NF -%' THEN replace("interpret", 'NF - ','')   ELSE "interpret" END
```

>![exo](images/icon_exo.png) *Exercice dirigé* **FACULTATIF**
>**Objectif:**écouvrir le langage ***regexp*** (les expressions régulières) avec une solution alternative pour mettre à jour le champ **"interpret"**
>```
>regexp_replace( "interpret", '\\w{2}\\s-\\s', '')`
>```

**explication de ~~texte~~ code:**

`\` : caractère d'échappement 

`\w` : correspond à un caractère compris dans les ensembles de lettres de A à Z en majuscule et en minuscule et dans les chiffres de 0 à 9

`{2}` : indique le nombre de caractères à récupérer

`\s` : correspond à une espace, avec caractère d'échappement

`-` : c’est le tiret de l’expression qu’on cherche

`\s` : à nouvel un espace avec caractère d’échappement




> Vous comprenez ici l'intérêt d’avoir des listes de valeurs dans l’inventaire, d’homogénéiser les valeurs et de penser votre système d’enregistrement en amont.



**![](images/attention.png)![](images/icon_important.png) SoUs Ce TeXtE , lA MiSe En pAgE eSt à FINIR / FIGNOLER / FINITIONNER ! ![](images/attention_color.png)![](images/icon_exo.png)**



### 1.3.3. Recherche de doublons attributaires

**Objectif:** Recherche de doublons sur le fichier **F103066_poly** 

#### 1.3.3.1. Installer l’extension Group Stat

> **Group Stat est l’équivalent du tableau croisé dynamique dans un tableur et fonctionne sur le même principe.**

**Vecteur ⇒ Group Stat**

Cliquer sur l'icône **Group Stat** 

1. Dans **Couches**, choisir la couche sur laquelle on veut synthétiser l’information: **F103066_poly**
2. Glisser le champ **numpoly** dans le panneau **Lignes**
3. Glisser le champ **numpoly** dans le panneau **Valeurs**
4. Glisser la fonction **compter** dans le panneau **Valeurs**
5. **Utiliser les valeurs NULL** (permet de vérifier que tous les enregistrements ont un numéro)
6. Cliquer sur le bouton **[Calculer]**
7. **Visualiser le résultat** et **Trier en ordre décroissant** pour visualiser les doublons ⇒ 10 doublons attributaires.

![group_stat](images/image047.png)

#### 1.3.3.2. Virtual Layer

> **Méthode SQL - Virtual layer :** 

**PRINCIPE** : `count ("*Champ à interroger*", group_by:="*Champ à interroger*") > 1`

```sqlite
SELECT numpoly, count(numpoly) as nombre_doublons FROM F103066_poly GROUP BY numpoly HAVING nombre_doublons > 1 ORDER BY nombre_doublons DESC
```
Nettoyer les données : supprimer les doublons.

### 1.3.4. Vérification de géométrie : Rappel

(vue dans les formations SIG 1 découverte et SIG 2 Figure)

La plupart des opérations effectuées sur des vecteurs ne fonctionnent pas sur des couches dont la géométrie n’est pas valide (auto-intersection/noeud papillon, noeud dupliqué…). Il faut donc vérifier et réparer, le cas échéant, les géométries non valides.

1. boîte de traitement ⇒ géométrie vectorielle ⇒ vérifier la validité
   ⇒ méthode QGIS ;

![group_stat](images/image048.png)

1. pour supprimer les noeuds en double : boîte de traitement, géométrie vectorielle, supprimer les sommets en double

![group_stat](images/image049.png)

1. pour les auto-intersections (papillons), sont détectés par l’outil “vérifier la validité” mais à corriger à la main

> **Methode SQL - Virtual layer :**


```sqlite
SELECT numpoly, ST_ISVALIDREASON(geometry) as erreur_geometrie, geometry as geom FROM F103066_poly WHERE ST_ISVALID(geometry) = 0
```

### 1.3.5. L’ordre des entités

**Jeux de données :** Chilleurs-aux-bois

**Objectif** : faire en sorte que l’empilement des entités géométriques corresponde à la réalité stratigraphique du site. 

Gestion par un ordre numérique stocké dans un champs.

>Charger les shp **F101055_ouverture** et **F101055_poly**. Ces couches sont déjà mises en forme. Ouvrir les propriétés de **F101055_ouverture** et faire remarquer l’ensemble de règles.
>Le style est enregistré par défaut (cf **F101055_ouverture.qml**).

**Remarque :** F101055_poly se rapporte aux faits archéologiques, mais le plan ne correspond pas à la réalité stratigraphique du site (faut croire le formateur sur parole !!!).
Plusieurs solutions : 
- à la main : créer un nouveau champ dans la table attributaire de la couche et rentrer l’ordre à la main. Rapide et efficace s’il y a peu de recoupements sur le site, mais laborieux et très difficile à définir si le site est complexe.
- en récupérant un ordre défini par une BdD en charge du traitement stratigraphique du site (ex : traitement sur Le Stratifiant).

>1. Charger le fichier **CADOC_fait_SIG.xls** dans le projet. 
>⇒   Ouvrir la table et remarquer le champs ordre. Changer l’encodage des données sources en **UTF-8**.
>2. Renommer la table CADOC_fait_SIG Feuille1 en **CADOC_fait_SIG**
>3. Depuis **F101055_poly** : faire une jointure avec **CADOC_fait_SIG**
>4. Appliquer un style pour visualiser le résultat avant tri.
>5. Enregistrer en dur la jointure
>  ![group_stat](images/image050.png)

**Méthode 1:** Trier les enregistrement selon Si elle n’est pas déjà installée, installer l’extension **MMQGIS**.

Puis Menu [MMQGIS] ⇒ Modify ⇒ Sort (trier)

![group_stat](images/image051.png)

![group_stat](images/image052.png)

1. **Source Layer  Name**: sélectionner la couche à traiter **F101055_poly**
2. **Sort Attribute** : choisir le champ contenant l’ordre numérique, ici “**J_ordre**”
3. **Direction** : croissant ou décroissant, ici choisir décroissant (**Descending**)
4. **Output File Name** : spécifier le nom et l’emplacement où va être enregistré le nouveau shp. Proposition : rajouter le suffixe ordre au nom de la couche d’origine ⇒ **F101055_poly_ordre**.
5. **OK** 

>Note: Pourquoi décroissant (Descending) ? Ce n’est pas forcément toujours le cas, mais, dans l’exemple utilisé ici, l’ordre numérique défini se base sur la position de l’unité d’enregistrement US/Fait au sein du diagramme stratigraphique. Les faits les plus récents se trouvent en haut du diagramme, donc sur les premières lignes (le fait le plus récent, d’un point de vue stratigraphique toujours, aura le numéro un) et les plus anciens sur les dernières lignes. Dans cet exemple, l’ordre a été obtenu à l’aide de l’application [Le Stratifiant](https://abp.hypotheses.org/3965).

>Si le contexte s’y prête, l’ordre peut être défini à la main en créant un nouveau champ dans la table attributaire de la couche désirée et en rentrant les numéros à la mains.

>MMQGis crée un nouveau shp. C’est mieux de faire ainsi et de ne pas écraser le shp d’origine. Comme ça, on peut toujours revenir aux données non traitées au cas où...

⇒ La nouvelle couche est ajoutée au projet. Observer le **changement dans l’empilement des entités**. Le plan devient "stratigraphiquement juste".

**Remarque :** les champs de la jointure sont intégrés en dur dans la table attributaire de la nouvelle couche.

![avant](images/image053.png)

**AVANT**

![après](images/image054.png)

**APRÈS**

Faire un style identique pour les deux couches **F101055_poly** et **F101055_poly_ordre** pour comparer plus facilement les deux.

On peut en profiter à ce moment pour modifier la symbologie de la nouvelle couche en mettant en évidence la périodisation (Remarque : ça permet de faire un rappel sur le style catégorisé).

> **Objectif:** mettre en évidence par la symbologie l'ordre stratigraphique des Faits.
>
> On choisit le style **catégorisé** car on a affaire à une **variable qualitative**.
>
> On utilise une **palette de dégradé** c’est à dire la **variable visuelle valeur** car la variable est **ordonnée**.
> 
> ![group_stat](images/image055.png)
> 
> Faire un style catégorisé sur le champs **J_periode**.
>
>Astuce: pour vérifier “visuellement” si l’ordre des entités défini est en correspondance avec la périodisation : utiliser la palette de couleur Reds. Les périodes anciennes sont claires et les périodes récentes sont foncées. **Observer et vérifier**. 

Avec un **virtual layer**, on réorganise la table des vestiges : 

```sqlite
SELECT numpoly, interpret, geometry, J_periode , J_ordre FROM F101055_poly_ordre ORDER BY J_periode ASC, J_ordre DESC
```

Quand on catégorise sur la période, **"periode"** et **"strati"** sont respectées.