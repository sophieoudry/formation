# Summary

## SIG perfectionnement 1: Analyses Spatiales
* [Avant-propos](/pas_a_pas/perf1_intro.md)

* [0 Reprise en main de QGIS](/pas_a_pas/perf1_0_reprise_en_main.md)
  * [Rappel des fondamentaux](/pas_a_pas/perf1_0_reprise_en_main.md#01-rappel-des-fondamentaux-des-sig-si-nécessaire-)
  * [Le dossier .qgis3](pas_a_pas/perf1_0_reprise_en_main.md#02-le-dossier-qgis3)
  * [Projection/Reprojection](/pas_a_pas/perf1_0_reprise_en_main.md#03-projection-reprojection)

* [1 Données spatiales et descriptives](/pas_a_pas/perf1_1_articulation_donnees.md)

	* [1.1 Conceptualisation de l'enregistrement](/pas_a_pas/perf1_1_1_MCD.md)
		* [Rappels historiographiques](/pas_a_pas/perf1_1_1_MCD.md#111-rappels-historiographiques-et-conceptuels)
		* [ Différentes approches](/pas_a_pas/perf1_1_1_MCD.md#112-plusieurs-approches-entrée-par-lobjet-lus-la-structure)
		* [La trilogie Fait-US-Artefact](/pas_a_pas/perf1_1_1_MCD.md#113-le-coeur-de-tout-système-denregistrement-archéologique-la-trilogie-fait-us-artefact)

	* [1.2 Outils existants pour exploiter donnée spatiale & descriptive](/pas_a_pas/perf1_1_2_formats.md)
		* [Stockage des données: shp et csv-wkt](/pas_a_pas/perf1_1_2_formats.md#121-les-différents-types-de-stockage-des-données-shp-csvwkt)
		* [Jointure](/pas_a_pas/perf1_1_2_formats.md#122-jointures)
		* [Les relations dans QGIS](/pas_a_pas/perf1_1_2_formats.md#123-les-relations-dans-qgis)
		* [SQLite/Spatialite](/pas_a_pas/perf1_1_2_formats.md#124-spatialite)
		* [PostGis (Caviar)](/pas_a_pas/perf1_1_2_formats.md#125-postgis-caviar)

## Fiches mémo
* [memo SQL](/memoSQL/memoSQL.md)