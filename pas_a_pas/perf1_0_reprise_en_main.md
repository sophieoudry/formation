
# Reprise en main de QGIS

Options du logiciel, encodage, paramétrage...

## 0.1. Rappel des fondamentaux des SIG (si nécessaire… )

paramétrage projet:

- paramétrages de QGIS pour les projets à venir
- paramétrage du projet en cours
- - vérifier SCR (2154)
  - chemins d’accès
  - demander le SCR pour les nouvelles couches

projection /reprojection → soit exporter, soit boîte à outils de traitement

## 0.2. Le dossier .qgis3

Le dossier **.qgis3** est créé par QGIS lors de son premier démarrage et comprend un certain nombre de dossiers « annexes » à l’installation, comme le dossier *project_templates* qui stocke les modèles que vous avez créés, le dossier *python* qui stocke les extensions chargées, etc…
Ce dossier peut être également alimenté avec vos propres fichiers ressources (symboles SVG, polices, …). 
Il est conseillé de mettre systématiquement les fichiers ressources (svg, modèles de mises en page, qml..) dans ce dossier.qgis3, afin de faciliter la sauvegarde de tous ces éléments, ainsi que leur utilisation dans des modèles.
Ce dossier est commun à toutes les versions successives de QGIS. Il ne doit donc pas être supprimé.

dans l’arborescence Windows
```
Windows 7 et 10 
C:\Users\compteUtilisateur\AppData\Roaming\QGIS\QGIS3\profiles
```
Par défaut, les extensions, les modèles, les modèles de composeur… sont situés dans ce répertoire. Il peut être copié d’un poste à l’autre pour conserver un certain nombre de réglages associés au logiciel.

Pour y accéder  depuis QGIS: Préférences > Profils utilisateurs > Ouvrir le dossier du profil actif

![profil_utilisateur](images/image002.png)

## 0.3. Projection / Reprojection

**Projection** : mise à plat d’une partie ou de la totalité de la surface courbe de la terre.

**Système de coordonnées de référence (SCR)** : modèle mathématique permettant, grâce aux coordonnées, de faire le lien entre un endroit réel sur terre et sa représentation sur plan. Le choix du SCR est important : il dépend de la taille de la zone de travail, des analyses que l’on veut en tirer… 

Dans le cadre de notre travail quotidien, il est fortement recommandé (obligatoire) de travailler en **RGF93/Lambert 93 (EPSG:2154) → système de projection légal en France.**

### 0.3.1 Projection à la volée

Pour activer/désactiver dans QGIS3 : Projet ⇒ Propriétés ⇒ SCR ⇒ cocher “aucune projection” pour désactiver la reprojection.

![projection_av](images/image003.png)



![attention](images/attention.png) La désactivation de la reprojection à la volée peut poser des problèmes dans le composeur d’impression (problème d’échelle, de graticules…). Il est donc fortement déconseillé de désactiver la reprojection à la volée.

### 0.3.2. Reprojection de couche

Rappel de SIG 1: Découverte:  

Il est possible que le fichier transmis par un collègue ne soit pas projeté dans le même SCR que le projet sur lequel vous travaillez (certains topos utilisent les SCR en conique conforme pour pallier les problématiques de déformation). Il convient dans ce cas d’effectuer une re-projection de couche (autrement dit, effectuer une transformation d’un SCR vers un autre SCR). Pour cela, le logiciel Qgis comprend des outils de re-projection.

> Menu Traitement ⇒ Boîte à outils ⇒ outils généraux pour les vecteurs ⇒ reprojeter une couche. 

