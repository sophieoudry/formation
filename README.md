# Supports des formations SIG & Stat

Ce dépôt permet la mise en ligne des sources (textes, images) des supports de formation SIG & Stat:

```mermaid
graph LR
A(SIG 1 Découverte) --> B(SIG 2: Figures)
B --> C(SIG perf 1: Analyses Spatiales)
B --> D(SIG perf 2:BDD Attributaire et Spatiale)
F[Stat 1: descriptives univariées]
E{Ateliers}
```

## SIG perfectionnement 1: Analyses Spatiales

* Ce support de formation (*mis à jour*) se trouve en version web à l'adresse [https://archeomatic.gitlab.io/formation](https://archeomatic.gitlab.io/formation)
* Le [diaporama](https://slides.com/archeomatic/perf_1)
* Le déroulé formateur  (*work in progress*) sous la forme d'un document [Google Docs](https://docs.google.com/document/d/1AvW4YIVAEPoMutsbhfaHz6HrtdAkk9frIZ_WszYxUQc/edit).
* Le [mémo SQL](https://archeomatic.gitlab.io/formation/memoSQL/memoSQL.html)

## SIG perfectionnement 2 : Base de Données spatiale et attributaire

* Il n'existe pas encore de support de formation ;(

* Le [diaporama](https://slides.com/archeomatic/perf_2)

## Ressources et documentation

* [Guide pratique CAVIAR](https://clem_f.gitlab.io/caviar/)
* [Guide pratique des 6 couches (à faire)](https://sites.google.com/a/inrap.fr/reseau-referents-sig/Guides/Les%206%20couches.pdf?attredirects=0&d=1)
* Note DST - organisation du NAS (à faire)