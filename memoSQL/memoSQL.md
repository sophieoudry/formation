# Do you speak SQL ?

[source](http://www.commentcamarche.net/contents/1062-le-langage-sql)

le **SQL** (*Structured Query Language*, traduisez *Langage de requêtes structuré*) est un **langage de définition de données**, **un langage de manipulation de données** et un langage de contrôle de données pour les bases de données relationnelles.

Le SQL est un **langage de définition de données**, c'est-à-dire qu'il permet de **créer des tables dans une base de données relationnelle**, ainsi que d'en **modifier** ou en **supprimer**.

Le SQL est un **langage de manipulation de données**, cela signifie qu'**il permet de sélectionner, insérer, modifier ou supprimer des données dans une table d'une base de données relationnelle**. 

Il est possible avec SQL de définir des permissions au niveau des utilisateurs d'une base de données.

> En fait, le langage SQL peut rendre tous les services que l’on peut demander à une BDD relationnelle: Créer la structure de la BDD (créer des tables, créer les champs, remplir les données mais aussi créer les liens entre les tables et SURTOUT interroger la BDD (C’est d’ailleurs essentiellement pour cela que nous allons l’utiliser). C’est ce que vous savez déjà tous faire (plus ou moins) sans forcément vous en rendre compte.

Toutes les BDD relationnelles (“dignes de ce nom”) comprennent le langage SQL.

![Attention](images/attention.png) Pour connaitre toutes les fonctions SQL il est utile de se référer au site [SQL.sh](https://sql.sh/)

Avant tout le SQL est donc un langage de requête structuré il respecte donc **une syntaxe générale** de type :

```sqlite
SELECT (liste des attributs) FROM (liste des tables) WHERE (Conditions)
```

* La partie `SELECT` indique les champs qui doivent apparaîtrent dans la réponse. 

  *Note: ces champs sont soit des champs existants dans les tables déclarées derrière la clause `FROM` soit le résultat d’un calcul. Il faudra dans ce cas leur donner un nom.*

* La partie `FROM` décrit les tables qui sont utilisées dans la requête. 

* La partie `WHERE` exprime les conditions, elle est optionnelle.

> Quand vous faites une sélection dans QGIS la fenêtre expression correspond aux conditions c’est à dire ce qui suit la clause `WHERE`. Il faut imaginer que le logiciel fait précéder votre expression de la phrase:
>     `SELECT * FROM (Couche_en_cours) WHERE`    
>         *Où `*`signifie tous les champs*.

Il est conseillé lors de la rédaction d'une requête SQL de commencer par écrire la syntaxe générale et de commencer par déclarer les tables qui vont être utilisées derrière la clause `FROM` , puis de déclarer les champs à utiliser/créer derrière la clause `SELECT` et enfin éventuellement de spécifier les conditions derrière la clause `WHERE`.

## Les clauses SQL
(clause = Partie d'un ordre SQL précisant un fonctionnement particulier)

| clause SQL | Description |
|:----------:|-------------|
| `SELECT`   | Précise les colonnes qui vont apparaître dans la réponse |
| `FROM`     | Précise la (ou les) table intervenant dans l’interrogation |
| `WHERE`    | Précise les conditions à appliquer sur les enregistrements. On peut utiliser :|
||  - des opérateurs de comparaison `=` `>` `<` `=` `<>`|
||  - des opérateurs logiques `AND` `OR` `NOT`|
||  - les prédicats `IN` `LIKE` `NULL` `ALL` `SOME` `ANY``EXISTS`...|
| `GROUP BY` | Précise la (ou les) colonne de regroupement |
| `HAVING`   | Précise la (ou les) condition associée à un regroupement |
| `ORDER BY` | Précise l’ordre dans lequel vont apparaître les lignes de la réponse : |
|| `ASC` : en ordre croissant |
|| `DESC` : en ordre décroissant |
| `LIMIT (n)`| Permet de limiter le calcul au *n* premiers enregistrements |
| `DISTINCT` | Permet d’éviter les redondances dans les résultats (il s’agit d’un option à la commande `SELECT`). |
| `COUNT()`  | Permet de compter le nombre d’enregistrements dans une table. |

## Les opérateurs de comparaisons

La clause `WHERE` est définie par **une condition qui s'exprime à l'aide d'opérateurs de comparaison et d'opérateurs logiques**.

| opérateur | description |
|:---------:|-------------|
| A `=` B | A égal B |
| A `LIKE` 'chaîne' | permet d’insérer des caractères jokers: |
|| `%` désignant 0 à plusieurs caractères quelconques |
|| `_` désignant un seul caractère |
| A `<>` B | A différent de B |
| A `<` B | A plus petit que (inférieur à) B |
| A `>` B | A plus grand que (inférieur à) B |
| A `<=` B | A inférieur ou égal à B |
| A `>=` B | A supérieur ou égal à B |
| A `BETWEEN` B `AND` C | A est compris entre B et C |
| A `IN` (B1, B2,...) | A appartient à liste de valeur (B1,B2,..) |

## Les opérateurs logiques

`OR` pour séparer deux conditions dont au moins une doit être vérifiée.  
`AND` pour séparer deux conditions qui doivent être vérifiées simultanément.  
`NOT` permet d'inverser une condition.  

![shéma AND OR NOT](images/and_or_not.png)

# le SQL dans QGIS

Dans QGIS le SQL est partout ! On l'utilise :
  * Pour faire une sélection par expression
  * Dans la calculatrice de champs
  * Dans le *Calculateur d'expression* ![epsilon](images/epsilon.png) éditer un style *Ensemble de règles* ou des *Étiquettes*

## Le gestionnaire BD

Dans QGIS, pour faire des requêtes SQL il est possible d'utiliser le gestionnaire de Base de Données (*DB manager*)

![Menu > Base de données > DBmanager...](images/menuBDD.png)

Vous pouvez faire des requêtes sur toutes les couches affichées dans le panneau couche (shapefiles, tableaux importés,...) et/ou vous connecter à une base de données (PostGis, SQlite...)
Pour cela, dans le panneau **Fournisseur de données**, cliquer sur la Base de données à requêter ou sur Couches virtuelles > Couches du projet pour requêter des shapefiles.

Pour faire une requête cliquer sur ![l'icône requête SQL](images/iconeSQL.png)

![gestionnaire de BDD - fenêtre requête SQL](images/GBDrequete.png)

> Pour faire une requête avec le gestionnaire de Base de Données il faut:   
>     **1.** choisir la BDD (ou Couches du projet)    
>     **2.** Cliquer sur l'icône ![l'icône requête SQL](images/iconeSQL.png)    
>     **3.** Taper la requête SQL    
>     **4.** Cliquer sur le bouton [Exécuter]

*Note: Pour sauvegarder le résultat sous forme de table ou de couche il faut cocher la case* ***Charger en tant que nouvelle couche***

##  Les couches virtuelles

Une autre possibiloté est d'utiliser les **couches virtuelles (*virtual layers*)** qui permet de créer une couche virtuelle à partir des couches présentes dans le projets, et d'une requête SQL.

Pour cela il suffit d'ajouter une couche virtuelle  ![l'icône virtual layer](images/iconeVL.png) 

**a compléter avec imprim ecran**

# Le SQL au quotidien

## Les commentaires

Pour commenter une requête SQL on fait préceder le commentaire avec `/*` et on le termine par `*/`

```sqlite
SELECT * /* sélectionne tous les champs */
FROM tabfait /* depuis la table tabfait */
WHERE "indent" /* commentaire au milieu */ LIKE 'Fossé'
```

> Les commentaires ne seront pas exécutés

## Sélectionner seulement certains champs

```sqlite
SELECT "indent", "num_fait"
FROM tabfait
LIMIT 4
```

>  Va renvoyer **les 4 premiers enregistrements** de la table en n’affichant que les champs “ident” et “numfait”

*Note: si le "champ" déclaré n'existe pas il sera créé sans données à l'intérieur*

## Les alias

```sqlite
SELECT  "num_fait" AS numpoly, "indent" AS interpret
FROM tabfait LIMIT 4
```
> Va renvoyer les 4 premiers enregistrements de la table en affichant le champ  “numfait” avec l’intitulé **numpoly ** le champ “ident” avec un nouvel intitulé interpret 

## Rechercher les doublons

```sqlite
SELECT "num_fait", count("num_fait") as nombre_doublons
FROM F103066_poly 
GROUP BY "num_fait" 
HAVING nombre_doublons > 1 
ORDER BY nombre_doublons DESC
```
> Va renvoyer la liste des occurrences du champ “numpoly” et un champ “nombre_doublons” avec le nombre d’occurence.

*Note: fonctionne aussi pour identifier les valeurs uniques.*

## Faire une jointure

```sqlite
SELECT *
FROM tabfait
JOIN tabus ON tabFait."num_fait" = tabUs."num_fait"
```
> Va renvoyer une table contenant tous les champs de la table fait et de la table us 

*Note: si les deux tables ont un champ de jointure ayant le même nom on peut utiliser `USING "nom_du_champ"`

```sqlite
SELECT *
FROM tabfait
JOIN tabus USING "num_fait" -- si tabfait et tabus ont un champs appellé "num_fait"
```

## Requête d’une relation de 1 à n

```sqlite
SELECT * 
FROM tabfait
JOIN tabus ON tabFait."num_fait" = tabUs."num_fait" 
WHERE tabus."descrip" LIKE '%calage%'
```
>Va renvoyer une table contenant les champs de la table fait pour lesquels le champ description de la table us contient le mot ‘calage’ et le champ “num_fait” un identifiant existant dans la table fait. 

** Ajouter une requête avec en plus tabMob**

# Le SQL dans l’espace

Comme vous le savez les couches que l’on manipule dans QGIS sont de “simples” **tables attributaires avec une colonne contenant la géométrie**, celle-ci est est  intitulée *geometry* elle peuvent donc être requêtée comme n’importe quelle autre table avec en plus la possibilité de prendre en compte ce champ *geometry*.
Parfois le SQL peut aussi servir à faire des requêtes spatiales on peut dans ce cas utiliser des fonctions de **Spatialite**.

![Attention](images/attention.png) Il est utile de se référer à la liste des [fonctions de Spatialite](http://www.gaia-gis.it/gaia-sins/spatialite-sql-4.3.0.html)

> On entend par requête, comme pour les requêtes SQL non-géométrique d’ailleurs, toute manipulation d’une ou plusieurs tables ayant comme résultat une table.    


```sqlite
SELECT "num_fait", "ident", "geometry"
FROM tabfait
WHERE "ident" LIKE 'Fossé'
```
> L'appel du champ *"geometry"* dans la clause `FROM` permet de renvoyer une couche et non pas seulement une table.

![Attention](images/attention.png) Dans les BDD Spatialite et PostGis le champ de géométrie est intitulé **"geom"** et dans les shapefiles il est intitulé **"geometry"**

## Les commentaires "signifiants"
Quand on utilise les **couches virtuelles**, dans une requête SQL certains `/*:commentaires*/ utilisés dans la clause `SELECT`avec ` peuvent être "signifiants".
Par exemple:
  * on peut indiquer le *type* d'un champ en y accolant un comentaire du type `/*:integer*/`, `/*:real*/` ou `/*:string*/`
  * on peut indiquer la *géométrie* et le *SCR* d'une couche en accolant derrière le *geom* un commentaire du type `/*:polygon:2154*/` ou `/*:points:4326*/`

## Interroger caviar 

Le Catalogue de Visualisation de l'Information Archéologique (CAVIAR) est stocké sur un serveur PostGis, on peut donc faire des requêtes SQL pour n'afficher qu'une partie des entités par exemple:

```sqlite
SELECT p.* 
FROM activite.prescription AS p, activite.communes AS c
WHERE st_intersects(p.geom, c.geom) and c."nom_com" = 'Esvres'
```

> Va renvoyer uniquement les prescriptions de la commune d'Esvres
>
> *Note: pour appeller une table/couche PostGis cela se fait sous la forme: `shema.couche`*
>
> Note: Cette requête est possible sans même ouvrir les couches dans QGIS il suffit de se connecter à Caviar (la BDD PostGis)*

Note: PostGis peut stocker des couches avec des géométries différentes, par exemple dans Caviar la couche PostGis uniteobservation stocke des des polygones et des points

```sqlite
SELECT *
FROM activite.uniteobservation
WHERE ST_ASTEXT(geom) ILIKE '%POLYGON%'
```
> Va renvoyer une couche contenant les polygones de la couche PostGis uniteobservation.

## Corriger et vérifier les géométries

On peut **vérifier la validité des géométries**  (nottament les noeuds en doubles, les auto-sécants, *ring closure inforcement*)

```sqlite
SELECT "numpoly", ST_ISVALIDREASON("geometry") AS "invalid_reason", "geometry"
FROM F103066_poly
WHERE ST_ISVALID("geometry") = 0
```

> Va renvoyer une couche contenant les entités ayant des géométries invalides et leur "raison d'invalidité" dans la colonne "invalid_reason" 

Pour **corriger des erreurs de géométries**, on peut utiliser la méthode du buffer 0 (tampon de taille zéro) qui recrée la géométrie mais permet d'éliminer les noeuds dupliqués (*dupicate node*) et les polygones avec auto-contact (*self-contact*).
Attention: cette méthode ne permet pas de corriger les polygones auto-seccants (papillons) à corriger avant, et les polygones de moins de 3 noeuds à corriger après !

```sqlite
SELECT *, ST_BUFFER(p.geometry, 0) as geometry
FROM F103066_poly as p
```
> Va renvoyer la même couche avec un tampon de 0

## Esvres: Requête 1 à n

```sqlite
SELECT DISTINCT p."Num_Fait" /*:int*/, p."geometry" /*:polygon:2154*/
FROM F103455_poly AS p
JOIN F103455_point ON "Num_fait" = "FAIT"
WHERE "Matiere" LIKE 'Verre'
```
> Va renvoyer les Faits (sous forme de polygones) qui contiennent des points dont la "Matiere" est du 'Verre'


```sqlite
SELECT DISTINCT p."Num_Fait" /*:int*/, p."geometry" /*:polygon:2154*/, count("N_PT") AS nb_objets
FROM F103455_poly AS p
JOIN F103455_point ON "Num_fait" = "FAIT"
WHERE "Matiere" LIKE 'Verre'
GROUP BY "FAIT"
ORDER BY nb_objets DESC
```
> Va renvoyer la même chose mais sans doublons de faits grâce à la clause `DISTINCT`

## Obernai: cercles proportionnels

Données sources: 
- F103066_ouverture: une couche contenant des sondages.
- Obernai_ceram_LTD.csv: un inventaire de lots de céramique contenant des quantités de céramique dans des sondages 

Etapes:
  * Il faut faire une table contenant la somme des NR par StSd 
  * Récupérer une couche de centroides des ouvertures
  * Faire une jointure des centroides d'ouverture avec la table contenant la somme des NR

**Attention: il faut préalablement enlever les noms de champs en double genre "fond"**

```sqlite
SELECT o.*, sum(NR) as som_NR /*:int*/ , ST_CENTROID(o.geometry) AS geometry /*:point:2154*/
FROM F103066_ouverture AS o
JOIN Obernai_ceram_LTD as c ON numouvert = StSd
GROUP BY StSd
```

## Obernai: diagramme origine-destination

Une 

```sqlite
SELECT 
FROM 
WHERE 
```

## Esvres: diagramme de remontage

```sqlite
SELECT DISTINCT "start", "end", st_X(p1.geometry) AS x_start, st_Y(p1.geometry) AS y_start, st_X(p2.geometry) AS x_end, st_Y(p2.geometry) AS y_end
FROM S324_3202
JOIN F103455_point p1
ON "start" = p1.UE
JOIN F103455_point p2
ON "end" = p2.UE
```

## Esvres: orientation d'un axe

Données sources: 
- F103455_axe: une couche d'axe

Objectif:
Créer un champ contenant l'azimuth (l'angle dans le plan horizontal entre la direction d'un objet et le nord) des axes .

```sqlite
SELECT Fait,  degrees(st_azimuth(st_startpoint(geometry),st_endpoint(geometry))) as azimuth
FROM F103455_axe
WHERE typaxe LIKE 'Lon'
```

## Esvres: projection de points sur un axe

Données sources: 
- F103455_point: une couche de points représentant des fragments d'objet pris en X,Y,Z
- F103455_axe: une couche d'axe

Objectif:
Projeter les points de la sépulture 24 sur l'axe longitudinal de la sépultur 324.

```sqlite
SELECT p.*, st_make_point(st_Line_Locate_Point(a.geometry,p.geometry)*st_length(a.geometry), "Z" /*:real*/) as geom /*:point:2154*/
FROM F103455_axe as  a, F103455_point as p
WHERE a."Faits"='324' AND a."typaxe"='Lon' AND p."FAIT"=324
```

## Alizay: maille

Données sources:
- mobilier_archeo: une couche de points représentant des fragments d'objet.
- grille_10 : une grille de polygone de 10 m de coté.    


```sqlite
    SELECT g.id, g.geometry as geom, count(*)
    FROM mobilier_archeo as m
    JOIN grille_10 as g ON ST_INTERSECTS(m.geometry, g.geometry)
    GROUP BY g.geometry
```
> Va renvoyer une grille de polygones avec le décompte des mobiliers par carré.

[lien2](#les-operateurs-de-comparaisons)
