![SQL_averti](images/image001.png)

# Avant-Propos

**Pré-requis**

- Avoir obligatoirement suivi la formation **SIG 1: Découverte** & **SIG 2: Figures**.
- Il est recommandé d'avoir suivi **Stat1: Statistiques descriptives univariées**
- Utiliser régulièrement et fréquemment QGIS
- Être capable de créer un jeu de données “propre”, tant sur le plan attributaire que géométrique.

**Objectifs principaux** 

- Articulation entre l’enregistrement descriptif et le SIG; concevoir/mettre en oeuvre le système d’enregistrement par rapport à la problématique
- Acquérir les bases de l’analyse spatiale.
- Représenter des données quantitatives et qualitatives.
- Maîtriser les bases du langage SQL.

**Objectifs secondaires**

- Acquérir des automatismes
- - Group Stat
  - Boîte à outils du menu traitement
- Savoir s’orienter dans le langage SQL
- - comprendre une requête
  - chercher de l’aide sur une fonction

**Supports**

Diaporama sur (https://slides.com/archeomatic/perf_1) ajouter [/live](https://slides.com/archeomatic/perf_1/live) pour la diffusion synchronisée et en direct lors de la formation. 
> Se déplacer dans le diaporama  avec les touches ↑↓→← du clavier et la touche [Echap.] pour voir l'ensemble.

Ce support de formation est en ligne (https://archeomatic.gitlab.io/formation) et est mis à jour sur un dépôt GitLab (https://gitlab.com/archeomatic/formation) sous licence [CC by NC ND](https://creativecommons.org/licenses/by-nc-nd/2.0/fr/)![CC](images/icon_CC.png)

**Jeux de données**

- **F103066_Obernai**: csv wkt ; nettoyage, consolidation des données ; cercles proportionnels ; diagrammes en oursin
- **F103455_Esvres** : Correction de géométrie / Script 6 couches / SQL requête / Calcul d’orientation / Ensemble de 
- **F101055_Chilleurs_aux_bois** : lecture
- **F108633_Saran** : sqlite / spatialite
- **F025228_Alizay** : analyse par maille
